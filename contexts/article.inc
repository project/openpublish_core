<?php

$context = array(
  'namespace' => 'openpublish',
  'attribute' => 'node_detail',
  'value' => 'article',
  'node' => array(
    '0' => 'article',
  ),
  'block' => array(
    'morelikethis_taxonomy' => array(
      'module' => 'morelikethis',
      'delta' => 'taxonomy',
      'weight' => 32,
      'region' => 'right',
      'status' => '0',
      'label' => 'More Like This Taxonomy Block',
      'type' => 'context_ui',
    ),
    'morelikethis_flickr' => array(
      'module' => 'morelikethis',
      'delta' => 'flickr',
      'weight' => 33,
      'region' => 'right',
      'status' => '0',
      'label' => 'More Like This Flickr Block',
      'type' => 'context_ui',
    ),
    'block_3' => array(
      'module' => 'block',
      'delta' => '3',
      'weight' => 34,
      'region' => 'right',
      'status' => '0',
      'label' => 'Right Block Square Ad',
      'type' => 'context_ui',
    ),
    'morelikethis_googlevideo' => array(
      'module' => 'morelikethis',
      'delta' => 'googlevideo',
      'weight' => 35,
      'region' => 'under_content',
      'status' => '0',
      'label' => 'More Like This Google Video Block',
      'type' => 'context_ui',
    ),
  ),
);