<?php

$context = array(
  'namespace' => 'openpublish',
  'attribute' => 'general',
  'value' => 'sitewide',
  'sitewide' => '1',
  'block' => array(
    'block_2' => array(
      'module' => 'block',
      'delta' => '2',
      'weight' => 32,
      'region' => 'header',
      'status' => '0',
      'label' => 'Top Banner Ad',
      'type' => 'context_ui',
    ),
    'block_1' => array(
      'module' => 'block',
      'delta' => '1',
      'weight' => -33,
      'region' => 'footer',
      'status' => '0',
      'label' => 'Credits',
      'type' => 'context_ui',
    ),
  ),
);