<?php

$context = array(
  'namespace' => 'openpublish',
  'attribute' => 'listing_pages',
  'value' => 'resources',
  'views' => array(
    '0' => 'resources',
  ),
  'block' => array(
    'block_3' => array(
      'module' => 'block',
      'delta' => '3',
      'weight' => 32,
      'region' => 'right',
      'status' => '0',
      'label' => 'Right Block Square Ad',
      'type' => 'context_ui',
    ),
    'views_most_viewed_by_node_type-block' => array(
      'module' => 'views',
      'delta' => 'most_viewed_by_node_type-block',
      'weight' => 33,
      'region' => 'right',
      'status' => '0',
      'label' => 'Most Viewed By Node Type',
      'type' => 'context_ui',
    ),
  ),
);