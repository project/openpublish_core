<?php

/* Context for a number of conent detail pages, triggered when ANY of them is active. */

$context = array(
  'namespace' => 'openpublish',
  'attribute' => 'multi',
  'value' => 'content_detail',
  'node' => array(
    '0' => 'blog',
    '1' => 'article',
    '2' => 'event',
    '3' => 'resource',
  ),
);