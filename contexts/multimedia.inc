<?php

$context = array(
  'namespace' => 'openpublish',
  'attribute' => 'listing_pages',
  'value' => 'multimedia',
  'views' => array(
    '0' => 'multimedia',
  ),
  'block' => array(
    'block_3' => array(
      'module' => 'block',
      'delta' => '3',
      'weight' => 32,
      'region' => 'right',
      'status' => '0',
      'label' => 'Right Block Square Ad',
      'type' => 'context_ui',
    ),
    'views_most_viewed_multimedia-block' => array(
      'module' => 'views',
      'delta' => 'most_viewed_multimedia-block',
      'weight' => 33,
      'region' => 'right',
      'status' => '0',
      'label' => 'Most Viewed Multimedia',
      'type' => 'context_ui',
    ),
  ),
);