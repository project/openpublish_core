<?php

$context = array(
  'namespace' => 'openpublish',
  'attribute' => 'listing_pages',
  'value' => 'articles',
  'views' => array(
    '0' => 'articles',
  ),
  'block' => array(
    'block_3' => array(
      'module' => 'block',
      'delta' => '3',
      'weight' => 32,
      'region' => 'right',
      'status' => '0',
      'label' => 'Right Block Square Ad',
      'type' => 'context_ui',
    ),
    'views_most_viewed_by_taxonomy-block' => array(
      'module' => 'views',
      'delta' => 'most_viewed_by_taxonomy-block',
      'weight' => 33,
      'region' => 'right',
      'status' => '0',
      'label' => 'Most Viewed By Taxonomy',
      'type' => 'context_ui',
    ),
    'views_most_commented_articles-block_1' => array(
      'module' => 'views',
      'delta' => 'most_commented_articles-block_1',
      'weight' => 34,
      'region' => 'right',
      'status' => '0',
      'label' => 'Most Commented Articles',
      'type' => 'context_ui',
    ),
  ),
);