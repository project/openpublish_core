<?php

$view = new view;
$view->name = 'events';
$view->description = 'List of Events';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->override_option('fields', array(
  'field_event_date_value' => array(
    'label' => '',
    'link_to_node' => 0,
    'label_type' => 'none',
    'format' => 'default',
    'multiple' => array(
      'group' => TRUE,
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_reversed' => FALSE,
    ),
    'exclude' => 0,
    'id' => 'field_event_date_value',
    'table' => 'node_data_field_event_date',
    'field' => 'field_event_date_value',
    'relationship' => 'none',
    'override' => array(
      'button' => 'Override',
    ),
  ),
  'title' => array(
    'label' => '',
    'link_to_node' => 1,
    'exclude' => 0,
    'id' => 'title',
    'table' => 'node',
    'field' => 'title',
    'relationship' => 'none',
  ),
  'field_teaser_value' => array(
    'label' => '',
    'link_to_node' => 0,
    'label_type' => 'none',
    'format' => 'default',
    'multiple' => array(
      'group' => TRUE,
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_reversed' => FALSE,
    ),
    'exclude' => 0,
    'id' => 'field_teaser_value',
    'table' => 'node_data_field_teaser',
    'field' => 'field_teaser_value',
    'relationship' => 'none',
  ),
));
$handler->override_option('sorts', array(
  'sticky' => array(
    'order' => 'DESC',
    'id' => 'sticky',
    'table' => 'node',
    'field' => 'sticky',
    'relationship' => 'none',
  ),
  'field_event_date_value' => array(
    'order' => 'ASC',
    'delta' => -1,
    'id' => 'field_event_date_value',
    'table' => 'node_data_field_event_date',
    'field' => 'field_event_date_value',
    'relationship' => 'none',
  ),
  'title' => array(
    'order' => 'ASC',
    'id' => 'title',
    'table' => 'node',
    'field' => 'title',
    'relationship' => 'none',
  ),
));
$handler->override_option('arguments', array(
  'date_argument' => array(
    'id' => 'date_argument',
    'table' => 'node',
    'field' => 'date_argument',
  ),
));
$handler->override_option('filters', array(
  'type' => array(
    'operator' => 'in',
    'value' => array(
      'event' => 'event',
    ),
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'type',
    'table' => 'node',
    'field' => 'type',
    'relationship' => 'none',
  ),
  'status' => array(
    'operator' => '=',
    'value' => 1,
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'status',
    'table' => 'node',
    'field' => 'status',
    'relationship' => 'none',
  ),
  'date_filter' => array(
    'operator' => '>=',
    'value' => array(
      'value' => array(
        'date' => '',
      ),
      'min' => '2009-03-22 00:00:00',
      'max' => '2009-03-22 00:00:00',
      'default_date' => 'now',
      'default_to_date' => '',
    ),
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'date_fields' => array(
      'node_data_field_event_date.field_event_date_value' => 'node_data_field_event_date.field_event_date_value',
    ),
    'date_method' => 'OR',
    'granularity' => 'day',
    'form_type' => 'date_text',
    'default_date' => 'now',
    'default_to_date' => '',
    'year_range' => '-3:+3',
    'id' => 'date_filter',
    'table' => 'node',
    'field' => 'date_filter',
    'relationship' => 'none',
  ),
));
$handler->override_option('access', array(
  'type' => 'none',
));
$handler->override_option('title', 'Events');
$handler->override_option('header_format', '1');
$handler->override_option('header_empty', 0);
$handler->override_option('items_per_page', 25);
$handler = $view->new_display('page', 'Page', 'page_1');
$handler->override_option('arguments', array(
  'date_argument' => array(
    'default_action' => 'ignore',
    'style_plugin' => 'default_summary',
    'style_options' => array(),
    'wildcard' => 'all',
    'wildcard_substitution' => 'All',
    'title' => '',
    'default_argument_type' => 'fixed',
    'default_argument' => '',
    'validate_type' => 'none',
    'validate_fail' => 'not found',
    'date_fields' => array(
      'node_data_field_event_date.field_event_date_value' => 'node_data_field_event_date.field_event_date_value',
    ),
    'year_range' => '2000:2020',
    'date_method' => 'AND',
    'granularity' => 'year',
    'id' => 'date_argument',
    'table' => 'node',
    'field' => 'date_argument',
    'override' => array(
      'button' => 'Use default',
    ),
    'relationship' => 'none',
    'default_options_div_prefix' => '',
    'default_argument_user' => 0,
    'default_argument_fixed' => '',
    'default_argument_php' => '',
    'validate_argument_node_type' => array(
      'blog' => 0,
      'article' => 0,
      'event' => 0,
      'feed' => 0,
      'feeditem' => 0,
      'irakli' => 0,
      'multimedia' => 0,
      'package' => 0,
      'page' => 0,
      'resource' => 0,
      'story' => 0,
    ),
    'validate_argument_node_access' => 0,
    'validate_argument_nid_type' => 'nid',
    'validate_argument_vocabulary' => array(
      '42' => 0,
      '43' => 0,
      '44' => 0,
      '41' => 0,
      '45' => 0,
      '46' => 0,
      '47' => 0,
      '48' => 0,
      '49' => 0,
      '50' => 0,
      '51' => 0,
      '52' => 0,
      '53' => 0,
      '54' => 0,
      '55' => 0,
      '56' => 0,
      '57' => 0,
      '58' => 0,
      '59' => 0,
      '60' => 0,
      '61' => 0,
      '62' => 0,
      '63' => 0,
      '64' => 0,
      '65' => 0,
      '66' => 0,
      '67' => 0,
      '68' => 0,
      '69' => 0,
      '70' => 0,
      '71' => 0,
      '72' => 0,
      '73' => 0,
      '74' => 0,
      '75' => 0,
      '76' => 0,
      '77' => 0,
      '78' => 0,
      '79' => 0,
      '80' => 0,
    ),
    'validate_argument_type' => 'tid',
    'validate_argument_node_flag_name' => '*relationship*',
    'validate_argument_node_flag_test' => 'flaggable',
    'validate_argument_node_flag_id_type' => 'id',
    'validate_argument_user_flag_name' => '*relationship*',
    'validate_argument_user_flag_test' => 'flaggable',
    'validate_argument_user_flag_id_type' => 'id',
    'validate_argument_php' => '',
  ),
));
$handler->override_option('header', '<div class="float-right"><a href="/rss/events"><img src="/misc/feed.png" /></a></div>
<div class="date-filter">
<form id="year-month" onsubmit="window.location=\'/events/\'+this.sel_year.value+\'-\'+this.sel_month.value; return false;">
   <input class=\'filter-submit\' type=\'submit\' value=\'Filter\'/>
   <select class=\'year\' name="sel_year">
      <option value=\'all\'>YEAR</option>
      <option value=\'2010\'>2010</option>
      <option value=\'2009\'>2009</option>
      <option value=\'2008\'>2008</option>
      <option value=\'2007\'>2007</option>
      <option value=\'2006\'>2006</option>
   </select>
   <select class=\'month\' name="sel_month">
      <option value=\'all\'>MONTH</option>
      <option value=\'1\'>Jan</option>
      <option value=\'2\'>Feb</option>
      <option value=\'3\'>Mar</option>
      <option value=\'4\'>Apr</option>
      <option value=\'5\'>May</option>
      <option value=\'6\'>Jun</option>
      <option value=\'7\'>Jul</option>
      <option value=\'8\'>Aug</option>
      <option value=\'9\'>Sep</option>
      <option value=\'10\'>Oct</option>
      <option value=\'11\'>Nov</option>
      <option value=\'12\'>Dec</option>
   </select>
</form>
</div>');
$handler->override_option('header_format', '2');
$handler->override_option('items_per_page', 10);
$handler->override_option('use_pager', '1');
$handler->override_option('path', 'events');
$handler->override_option('menu', array(
  'type' => 'none',
  'title' => '',
  'description' => '',
  'weight' => 0,
  'name' => 'navigation',
));
$handler->override_option('tab_options', array(
  'type' => 'none',
  'title' => '',
  'description' => '',
  'weight' => 0,
));
$handler = $view->new_display('feed', 'Feed', 'feed_1');
$handler->override_option('style_plugin', 'rss');
$handler->override_option('style_options', array(
  'mission_description' => FALSE,
  'description' => '',
));
$handler->override_option('row_plugin', 'node_rss');
$handler->override_option('row_options', array(
  'item_length' => 'default',
));
$handler->override_option('path', 'rss/events');
$handler->override_option('menu', array(
  'type' => 'none',
  'title' => '',
  'description' => '',
  'weight' => 0,
  'name' => 'navigation',
));
$handler->override_option('tab_options', array(
  'type' => 'none',
  'title' => '',
  'description' => '',
  'weight' => 0,
));
$handler->override_option('displays', array());
$handler->override_option('sitename_title', FALSE);
$handler = $view->new_display('block', 'Homepage Block', 'block_1');
$handler->override_option('title', '');
$handler->override_option('items_per_page', 2);
$handler->override_option('use_more', 1);
$handler->override_option('block_description', 'Homepage Events');
$handler->override_option('block_caching', -1);